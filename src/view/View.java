package view;

import model.History;
import presenter.Presenter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class View extends JFrame implements IView {

    static JFrame jFrame;
    static JPanel jPanel;
    static String textPanel = " ";
    private static Presenter presenter;
    static String textFieldValue = " ";
    static JTextField jTextField = new JTextField();
    private static JFrame jFrameHistory;
    static JPanel jPanelHistory;
    History history;
    static JTextArea jTextArea = new JTextArea();

    public View() {
        history = History.getInstance();
    }

    public static void main(String[] args) {

        presenter = new Presenter(new View());

        View view = new View();
        JLabel jLabel=new JLabel();
        jFrame = new JFrame("Calculator");
        jFrameHistory=new JFrame("History");
        jPanel = new JPanel();
        jPanel.setLayout(null);

        jPanelHistory=new JPanel();
 //       jFrameHistory.add(jPanelHistory);
        jFrameHistory.add(jTextArea);

        jPanelHistory.add(jLabel);
        jFrameHistory.revalidate();
        jFrameHistory.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        jFrameHistory.setSize(500,700);
        jFrameHistory.setLocationRelativeTo(null);
        jFrameHistory.setResizable(false);




        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(300,400);
        jFrame.setLocationRelativeTo(null);
        jFrame.setResizable(false);
        jFrame.add(jPanel);

        JMenuBar jMenuBar = new JMenuBar();
        JMenu file = new JMenu("File");

        JMenu history = new JMenu("History");

        jMenuBar.add(file);
        jMenuBar.add(history);
        jFrame.setJMenuBar(jMenuBar);
        jFrame.revalidate();




        file.add(new JMenuItem("Save")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    presenter.getRecord1(textPanel);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });


        file.add(new JMenuItem("Open")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                int ret = fileopen.showDialog(null, "Выберите файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileopen.getSelectedFile();
                    FileReader fr = null;
                    try {
                        fr = new FileReader(file);
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                    Scanner scan = new Scanner(fr);
                    while (scan.hasNextLine()) {
                        String temp;
                        temp = scan.nextLine();

                    }
                    try {
                        fr.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        history.add(new JMenuItem("Open")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                jFrameHistory.setVisible(true);

                ArrayList<String> List = History.getInstance().getArray();
                String[] temp = new String[List.size()];

//                for (String s : List) {
//                    jTextArea.append(s.toString() + "\n"); // New line at the end
//                }
                for (int i = 0; i < List.size(); i++) {
                    jTextArea.append(List.get(i)+ "\n");
                }
//
//            }
//                for (int i = 0; i <= List.size(); i++) {
//                    temp[i] = List.get(i);
//                    jLabel.setText(temp[i]);
//                }
//                for (int i = 0; i <= temp.length; i++) {
//                   jTextArea.setText(temp[i]);
//                }
//            }


            }
        });

        history.add(new JMenuItem("Clear")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                History.getInstance().getArray().clear();
            }
        });

        jTextField.setBounds(5, 5, 275, 40);
        jTextField.setHorizontalAlignment(4);

        jPanel.add(jTextField);


        JButton jButtonBackspace = new JButton("←");
        jButtonBackspace.setBounds(20, 100, 50, 30);
        jPanel.add(jButtonBackspace);
        jButtonBackspace.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel = textPanel.substring(0, textPanel.length() - 1);
                jTextField.setText(textPanel);
            }
        });

        JButton jButtonC = new JButton("C");
        jButtonC.setBounds(90, 50, 50, 30);
        jPanel.add(jButtonC);
        jButtonBackspace.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                jTextField.setText(textFieldValue);
            }
        });

        JButton jButton0 = new JButton("0");
        jButton0.setBounds(20, 300, 120, 30);
        jPanel.add(jButton0);
        jButton0.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "0";
                jTextField.setText(textPanel);
            }
        });

        JButton jButton1 = new JButton("1");
        jButton1.setBounds(20, 250, 50, 30);
        jPanel.add(jButton1);
        jButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "1";
                jTextField.setText(textPanel);
            }
        });


        JButton jButton2 = new JButton("2");
        jButton2.setBounds(90, 250, 50, 30);
        jPanel.add(jButton2);
        jButton2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "2";
                jTextField.setText(textPanel);
            }
        });
        JButton jButton3 = new JButton("3");
        jButton3.setBounds(160, 250, 50, 30);
        jPanel.add(jButton3);
        jButton3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "3";
                jTextField.setText(textPanel);
            }
        });
        JButton jButton4 = new JButton("4");
        jButton4.setBounds(20, 200, 50, 30);
        jPanel.add(jButton4);
        jButton4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "4";
                jTextField.setText(textPanel);
            }
        });
        JButton jButton5 = new JButton("5");
        jButton5.setBounds(90, 200, 50, 30);
        jPanel.add(jButton5);
        jButton5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "5";
                jTextField.setText(textPanel);
            }
        });
        JButton jButton6 = new JButton("6");
        jButton6.setBounds(160, 200, 50, 30);
        jPanel.add(jButton6);
        jButton6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "6";
                jTextField.setText(textPanel);
            }
        });
        JButton jButton7 = new JButton("7");
        jButton7.setBounds(20, 150, 50, 30);
        jPanel.add(jButton7);
        jButton7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "7";
                jTextField.setText(textPanel);
            }
        });
        JButton jButton8 = new JButton("8");
        jButton8.setBounds(90, 150, 50, 30);
        jPanel.add(jButton8);
        jButton8.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "8";
                jTextField.setText(textPanel);
            }
        });
        JButton jButton9 = new JButton("9");
        jButton9.setBounds(160, 150, 50, 30);
        jPanel.add(jButton9);
        jButton9.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "9";
                jTextField.setText(textPanel);
            }
        });

        JButton jButtonDot = new JButton(".");
        jButtonDot.setBounds(160, 300, 50, 30);
        jPanel.add(jButtonDot);
        jButtonDot.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += ".";
                jTextField.setText(textPanel);
            }
        });
        JButton jButtonPlus = new JButton("+");
        jButtonPlus.setBounds(230, 150, 50, 80);
        jPanel.add(jButtonPlus);
        jButtonPlus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "+";
                jTextField.setText(textPanel);
            }
        });
        JButton jButtonMinus = new JButton("-");
        jButtonMinus.setBounds(230, 100, 50, 30);
        jPanel.add(jButtonMinus);
        jButtonMinus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "-";
                jTextField.setText(textPanel);
            }
        });
        JButton jButtonMultiply = new JButton("*");
        jButtonMultiply.setBounds(160, 100, 50, 30);
        jPanel.add(jButtonMultiply);
        jButtonMultiply.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "*";
                jTextField.setText(textPanel);
            }
        });
        JButton jButtonDivide = new JButton("/");
        jButtonDivide.setBounds(90, 100, 50, 30);
        jPanel.add(jButtonDivide);
        jButtonDivide.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textPanel += "/";
                jTextField.setText(textPanel);
            }
        });
        JButton jButtonPosOrNeg = new JButton("±");
        jButtonPosOrNeg.setBounds(230, 50, 50, 30);
        jPanel.add(jButtonPosOrNeg);
        jButtonPosOrNeg.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                textPanel += "±";
//                jTextField.setText(textPanel);

                presenter.getInfo3(textPanel);
                textPanel = jTextField.getText();
                jTextField.setText(textPanel);
            }
        });
        JButton jButtonEqual = new JButton("=");
        jButtonEqual.setBounds(230, 250, 50, 80);
        jPanel.add(jButtonEqual);
        jButtonEqual.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                presenter.getInfo1(textPanel);
                textPanel = jTextField.getText();
                jTextField.setText(textPanel);

            }
        });

        JButton jButtonSqrt = new JButton("√");
        jButtonSqrt.setBounds(160, 50, 50, 30);
        jPanel.add(jButtonSqrt);
        jButtonSqrt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                textPanel += "√";
//                jTextField.setText(textPanel);
                presenter.getInfo2(textPanel);
                textPanel = jTextField.getText();
                jTextField.setText(textPanel);


            }
        });
        jFrame.revalidate();
        jFrame.setVisible(true);
    }

    @Override
    public void showResult(String massage) {
        jTextField.setText(massage);
    }

    @Override
    public void showMassage(String massage) {

    }

    @Override
    public void record(ArrayList<String> s) {

    }

    @Override
    public void showMassage(ArrayList<String> massage) {

    }

}