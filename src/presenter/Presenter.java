package presenter;

import model.Model;
import view.IView;

import java.io.IOException;
import java.util.ArrayList;


public class Presenter implements IPresenter {
    private Model model;
    private IView view;


    public Presenter(IView view) {
        this.view = view;
        this.model = new Model();
    }

    @Override
    public void getInfo1(String string) {
        view.showResult(model.getResult(string));
    }

    @Override
    public void getInfo2(String string) {
        view.showResult(model.Sqrt(string));

    }

    @Override
    public void getInfo3(String string) {
        view.showResult(model.ChangeSign(string));
    }

    @Override
    public void getRecord1(String string)throws IOException  {
        view.showMassage(model.saveFile(string));
    }



//    @Override
//    public void getRecord2(String string) throws IOException {
//        view.showMassage(model.openFile());
//    }

    @Override
    public void getRecord3(ArrayList<String > list) throws IOException {
        view.record(model.openHistory(list));
    }

    @Override
    public void getRecord4(ArrayList<String > list) throws IOException {
        view.record(model.clearHistory(list));
    }

}