package presenter;


import java.io.IOException;
import java.util.ArrayList;

public interface IPresenter {
    void getInfo1(String string);
    void getInfo2(String string);
    void getInfo3(String string);


    void getRecord1(String string)throws IOException;
//    void  getRecord2(String string) throws IOException;


    void getRecord3(ArrayList<String> list) throws IOException;
    void getRecord4(ArrayList<String> list) throws IOException;
}
