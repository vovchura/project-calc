package model;


import java.io.IOException;
import java.util.ArrayList;

public interface IModel {

    String getResult(String string);
    String Sqrt(String string);
    String ChangeSign(String string);

    String saveFile(String string) throws IOException;
//    String openFile ();

    ArrayList<String> openHistory(ArrayList<String> list) throws IOException;
    ArrayList<String> clearHistory(ArrayList<String> list) throws IOException;
}