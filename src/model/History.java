package model;

import java.util.ArrayList;

public  class History {
    private static History history;

    private static ArrayList<String> list ;

    public static History getInstance() {
        if (history == null)
            history = new History();

         return history;
    }

    private  History() {
        list = new ArrayList<String>();
    }

    // retrieve array from anywhere
    public ArrayList<String> getArray() {
        return this.list;
    }

    //Add element to array
    public static void addToArray(String value) {
        list.add(value);
    }
}
