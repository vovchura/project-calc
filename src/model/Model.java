package model;


import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Model implements IModel {

    double i1;
    double i2;
    String nstring = null;
    String buffString = null;
    History history;

 //   private static ArrayList<String> list;

    public Model() {
        history = History.getInstance();
    }


    @Override
    public String getResult(String string) {


        if (string.contains("-")) {
            String[] parts = string.split("[-]");
            String part1 = parts[0];
            String part2 = parts[1];

            if (part1.contains(("."))) {
                i1 = Double.parseDouble(part1.trim());
            } else {
                i1 = Integer.parseInt(part1.trim());

            }
            if (part2.contains(("."))) {
                i2 = Double.parseDouble(part2.trim());
            } else {
                i2 = Integer.parseInt(part2.trim());
            }
            nstring = String.valueOf(i1 - i2);
            //           System.out.println(i1 - i2);
            History.addToArray(string + "=" + nstring);
        }
        if (string.contains("/")) {
            String[] parts = string.split("[/]");
            String part1 = parts[0];
            String part2 = parts[1];

            if (part1.contains(("."))) {
                i1 = Double.parseDouble(part1.trim());
            } else {
                i1 = Integer.parseInt(part1.trim());

            }
            if (part2.contains(("."))) {
                i2 = Double.parseDouble(part2.trim());
            } else {
                i2 = Integer.parseInt(part2.trim());
            }
            nstring = String.valueOf(i1 / i2);
            History.addToArray(string + "=" + nstring);
        }
        if (string.contains("+")) {
            String[] parts = string.split("[+]");
            String part1 = parts[0];
            String part2 = parts[1];

            if (part1.contains(("."))) {
                i1 = Double.parseDouble(part1.trim());
            } else {
                i1 = Integer.parseInt(part1.trim());

            }
            if (part2.contains(("."))) {
                i2 = Double.parseDouble(part2.trim());
            } else {
                i2 = Integer.parseInt(part2.trim());
            }
            nstring = String.valueOf(i1 + i2);
            History.addToArray(string + "=" + nstring);
        }


        if (string.contains("*")) {
            String[] parts = string.split("[*]");
            String part1 = parts[0];
            String part2 = parts[1];

            if (part1.contains(("."))) {
                i1 = Double.parseDouble(part1.trim());
            } else {
                i1 = Integer.parseInt(part1.trim());

            }
            if (part2.contains(("."))) {
                i2 = Double.parseDouble(part2.trim());
            } else {
                i2 = Integer.parseInt(part2.trim());
            }
            nstring = String.valueOf(i1 * i2);
            History.addToArray(string + "=" + nstring);
        }

        return nstring;

    }

    @Override
    public String Sqrt(String string) {
        double temp = Double.parseDouble(string.trim());
        temp = Math.sqrt(temp);
        nstring = Double.toString(temp);
        History.addToArray(nstring);
        return nstring;

    }

    @Override
    public String ChangeSign(String string) {
        double temp = Double.parseDouble(string.trim());
        temp *= -1;
        nstring = Double.toString(temp);
        History.addToArray(nstring);
        return nstring;
    }

    @Override
    public String saveFile(String string) throws IOException {
        File file = new File("History.txt");
        file.createNewFile();
        FileWriter fw = new FileWriter(file);
        for (int i = 0; i < history.getArray().size(); i++) {
            fw.write(history.getArray().get(i) + "\n");
        }
        fw.flush();
        fw.close();

        return string;
    }

//    @Override
//    public String openFile() {
//        JFileChooser fileopen = new JFileChooser();
//        int ret = fileopen.showDialog(null, "Выберите файл");
//        if (ret == JFileChooser.APPROVE_OPTION) {
//            File file = fileopen.getSelectedFile();
//            FileReader fr = null;
//            try {
//                fr = new FileReader(file);
//            } catch (FileNotFoundException e1) {
//                e1.printStackTrace();
//            }
//            Scanner scan = new Scanner(fr);
//            while (scan.hasNextLine()) {
//                String temp;
//                temp = scan.nextLine();
//
//            }
//            try {
//                fr.close();
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            }
//        }
//   return nstring;
//    }
    @Override
    public ArrayList<String> openHistory(ArrayList<String> list) throws IOException {

        return list;
    }
    @Override
    public ArrayList<String> clearHistory(ArrayList<String> list) throws IOException {
        list = null;
        return list;
    }
}